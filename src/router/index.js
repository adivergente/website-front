import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import { langs } from '@/locales'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/default'),
    // beforeEnter: langMiddleware,
    children: [
      {
        path: '/',
        name: 'Home',
        component: Home
      },
      {
        path: 'productos/p',
        name: 'ProductoDetalles',
        component: () => import(/* webpackChunkName: "Detalles" */ '@/views/ProductoDetalles.vue')
      },
      {
        path: 'productos',
        name: 'Productos',
        component: () => import(/* webpackChunkName: "Productos" */ '@/views/Productos.vue')
      },
      {
        path: 'noticias/:noticia',
        name: 'NoticiaDetalles',
        component: () => import(/* webpackChunkName: "NoticiaDetalles" */ '@/views/NoticiaDetalles.vue')
      },
      {
        path: 'noticias',
        name: 'Noticias',
        component: () => import(/* webpackChunkName: "Noticias" */ '@/views/Noticias.vue')
      },
      {
        path: 'promociones/:promocion',
        name: 'PromocionDetalles',
        component: () => import(/* webpackChunkName: "PromocionDetalles" */ '@/views/PromocionDetalles.vue')
      },
      {
        path: 'promociones',
        name: 'Promociones',
        component: () => import(/* webpackChunkName: "Promociones" */ '@/views/Promociones.vue')
      },
      {
        path: 'cotizacion',
        name: 'Cotizacion',
        component: () => import(/* webpackChunkName: "Cotizacion" */ '@/views/Cotizacion.vue')
      },
      {
        path: 'login',
        name: 'Login',
        component: () => import(/* webpackChunkName: "LoginRegister" */ '@/views/auth/LoginRegister.vue')
      },
      {
        path: 'registro',
        name: 'Registro',
        component: () => import(/* webpackChunkName: "LoginRegister" */ '@/views/auth/LoginRegister.vue')
      },
      {
        path: 'password-reset/:token',
        name: 'ResetPassword',
        component: () => import(/* webpackChunkName: "ResetPassword" */ '@/views/auth/ResetPassword.vue')
      },
      {
        path: 'password-reset',
        name: 'ForgotPassword',
        component: () => import(/* webpackChunkName: "ForgotPassword" */ '@/views/auth/ForgotPassword.vue')
      },
      {
        path: 'mi-cuenta',
        name: 'MiCuenta',
        component: () => import(/* webpackChunkName: "MiCuenta" */ '@/views/user/MiCuenta.vue'),
        meta: {
          authenticated: true
        }
      },
      {
        path: 'cart/checkout',
        name: 'Checkout',
        component: () => import(/* webpackChunkName: "Checkout" */ '@/views/cart/Checkout.vue')
      },
      {
        path: 'cart/resumen',
        name: 'CartResumen',
        component: () => import(/* webpackChunkName: "CartResumen" */ '@/views/cart/Resumen.vue'),
        meta: {
          authenticated: true
        }
      },
      {
        path: 'cart/confirmacion',
        name: 'PaymentConfirmation',
        component: () => import(/* webpackChunkName: "PaymentConfirmation" */ '@/views/cart/Confirmacion.vue'),
        meta: {
          authenticated: true
        }
      },
      {
        path: 'aviso-de-privacidad',
        name: 'AvisoDePrivacidad',
        component: () => import(/* webpackChunkName: "AvisoDePrivacidad" */ '@/views/disclaimer/AvisoDePrivacidad.vue')
      },
      {
        path: 'garantias-y-devoluciones',
        name: 'Garantias',
        component: () => import(/* webpackChunkName: "Garantias" */ '@/views/disclaimer/Garantias.vue')
      },
      {
        path: 'uso-de-cookies',
        name: 'UsoDeCookies',
        component: () => import(/* webpackChunkName: "UsoDeCookies" */ '@/views/disclaimer/UsoDeCookies.vue')
      },
      {
        path: '*',
        component: () => import(/* webpackChunkName: "404" */'@/views/404.vue')
      }
    ]
  }
]

/**
 * Extraer prefijo de idioma
 */
const locale = window.location.pathname.replace(/^\/([^/]+).*/i, '$1')
/**
 * Verificar que sea un prefijo existente en las traducciones
 * y que no sea es (lang por defecto)
 */
const lc = langs.includes(locale) && locale !== 'es' ? locale : ''

/**
 * Inicializar router
 * Param base agrega prefijo de lang a todas las rutas
 */
const router = new VueRouter({
  mode: 'history',
  base: (locale.trim().length && locale !== '/') ? '/' + lc : undefined,
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => {
  // checkLocale(to, from, next)
  // verificar si la ruta requiere estar autenticado
  // const lc = window.location.pathname.replace(/^\/([^/]+).*/i, '$1')
  // console.log('lcale in router', lc)
  // console.log(process.env.BASE_URL)
  if (to.matched.some(route => route.meta.authenticated)) {
    if (localStorage.getItem('token')) {
      next()
    } else {
      next({ name: 'Login' })
    }
  } else {
    // si esta logueado e intenta ir a login, redireccionar a inicio
    if (to.name === 'Login' && localStorage.getItem('token')) {
      next({ name: 'MiCuenta' })
    } else {
      next()
    }
  }
})

// function langMiddleware (to, from, next) {
//   console.log('to', to, i18n.locale)
//   const availableLangs = ['es', 'en']
//   if (!availableLangs.includes(to.params.locale)) {
//     console.log('contains')
//     next('/es')
//   }
//   next()
// }

export default router
