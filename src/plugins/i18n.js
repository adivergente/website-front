import Vue from 'vue'
import VueI18n from 'vue-i18n'

import vEn from 'vuetify/lib/locale/en'
import vEs from 'vuetify/lib/locale/es'

import { es, en, langs } from '@/locales'

Vue.use(VueI18n)

const messages = {
  en: {
    ...en,
    $vuetify: vEn
  },
  es: {
    ...es,
    $vuetify: vEs
  }
}

/**
 * Setear locale en caso de existir en lista de lang dsiponibles, sino setear por defecto 'es'
 */
const lc = window.location.pathname.replace(/^\/([^/]+).*/i, '$1')
const locale = langs.includes(lc) ? lc : ''
export default new VueI18n({
  locale: (locale.trim().length && locale !== '/') ? locale : 'es',
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'es',
  messages
})
