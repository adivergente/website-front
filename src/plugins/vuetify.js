import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import es from 'vuetify/es5/locale/es'
import '@/assets/scss/main.scss'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#366CB3'
      }
    }
  },
  lang: {
    locales: { es },
    current: 'es'
  }
})
