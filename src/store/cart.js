import dayjs from 'dayjs'
import Repository from '@/api/Repository'
const User = Repository.get('user')

export default {
  namespaced: true,
  state: {
    productos: JSON.parse(localStorage.getItem('cart')) || [],
    paqueteria: {
      precio: 0 // Se setea en comit SET_PRECIO_PAQUETERIA llamado en TotalCartResume -> adjustShippingPrice
    },
    envio: {},
    formasPago: [],
    paymentForm: 'paypal',
    paymentError: false,
    savingOrder: false,
    showBuyMessage: false
  },
  getters: {
    total (state) {
      let total = 0
      state.productos.forEach(({ product, qty }) => {
        if (product.id_promo !== '0' && product.promocion.length && product.promocion[0].descuento) {
          total += qty * (product.precio - (product.precio * (product.promocion[0].descuento / 100)))
        } else {
          total += qty * product.precio
        }
        // console.log('prod y getter', product)
      })
      // console.log('total', total)
      return Math.round(total * 100) / 100
    },
    totalProducts (state) {
      let total = 0
      state.productos.forEach(({ qty }) => {
        total += qty
      })
      return total
    },
    orderTotal (state, getters) {
      return Math.round((getters.total + state.paqueteria.precio + Number.EPSILON) * 100) / 100
    },
    orderDescription (state) {
      const prods = []
      state.productos.forEach(obj => {
        prods.push(`${obj.qty}x${obj.product.nombre}`)
      })
      return prods.join(', ')
    },
    orderStatus (state) {
      return state.paymentForm === 'paypal'
        ? 'pagado'
        : 'pendiente'
    },
    hasShippingInfo (state) {
      return !!state.envio.calle && !!state.envio.num && !!state.envio.colonia && !!state.envio.municipio && !!state.envio.estado && !!state.envio.codigo_postal
    },
    shippingInfo (state) {
      return state.envio
    },
    paqueteria (state) {
      return state.paqueteria
    },
    getPaymentForm (state) {
      return state.paymentForm
    },
    getShowBuyMessage (state) {
      return state.showBuyMessage
    }
  },
  mutations: {
    ADD_TO_CART (state, { product, qty }) {
      const prod = state.productos.find(item => item.product._id === product._id)

      if (prod) {
        prod.qty = qty
        return
      }
      state.productos.push({
        product,
        qty
      })
    },
    REMOVE_FROM_CART (state, index) {
      /**
       * Splice(posicion, elems-a-remover)
       * devuelve el elemento removido
       * el array original se queda sin el elemento removido y es reindexado
       */
      state.productos.splice(index, 1)
    },
    SET_CART (state, products) {
      state.productos = products
    },
    UPDATE_FROM_LOCALSTORAGE (state) {
      /**
       * Seteamos los valor al nuevo valor en localstorage
       * en caso de no existir(esto pasa al vaciar el carrito), ponemos valor por defecto
       */
      state.productos = JSON.parse(localStorage.getItem('cart')) || []
    },
    SET_SHIPPING_INFO (state, shipping) {
      state.envio = shipping
    },
    SET_FORMAS_PAGO (state, fPago) {
      state.formasPago = fPago
    },
    SET_PAYMENT_FORM (state, paymentForm) {
      state.paymentForm = paymentForm
    },
    SET_PAYMENT_ERROR (state, payload) {
      state.paymentError = payload
    },
    SET_PRECIO_PAQUETERIA (state, precio) {
      state.paqueteria.precio = precio
    },
    SET_SAVING_ORDER (state, payload) {
      state.savingOrder = payload
    },
    SET_BUY_MESSAGE (state, payload) {
      state.showBuyMessage = payload
    }
  },
  actions: {
    addToCart ({ commit, dispatch }, { product, qty }) {
      commit('ADD_TO_CART', { product, qty })
      dispatch('updateLocalStorage')
    },
    removeProduct ({ state, commit, dispatch }, product) {
      const indexToRemove = state.productos.map(item => item.product._id).indexOf(product._id)
      commit('REMOVE_FROM_CART', indexToRemove)
      dispatch('updateLocalStorage')
    },
    unsetCart ({ commit, dispatch }) {
      commit('SET_CART', [])
      dispatch('updateLocalStorage')
    },
    updateLocalStorage ({ state, getters }) {
      // console.log('total desde update:', getters.totalProducts)
      if (getters.totalProducts) {
        localStorage.setItem('cart', JSON.stringify(state.productos))
      } else {
        localStorage.removeItem('cart')
      }
    },
    setShippingInfo ({ commit }, shipping) {
      commit('SET_SHIPPING_INFO', shipping)
    },
    async paymentSuccess ({ commit, dispatch, rootState }, data) {
      // console.log('check user', rootState.auth.user)
      /**
       * Crear orden
       * Los datos del usuario se agregan en back mediante el token de sesion
       */
      commit('SET_SAVING_ORDER', true)
      const order = await dispatch('createOrder', data)
      console.log('order', order)
      try {
        const { success, message } = await User.saveOrder(order, rootState.auth.token)
        if (await success) {
          // resetear datos del carrito
          commit('SET_CART', [])
          commit('SET_PAYMENT_ERROR', false)
          commit('SET_SAVING_ORDER', false)
          commit('SET_BUY_MESSAGE', true)
          dispatch('setShippingInfo', {})
          dispatch('updateLocalStorage')
          return {
            success: await success,
            message: await message
          }
        }
        // mostrar error
        commit('SET_PAYMENT_ERROR', true)
        commit('SET_SAVING_ORDER', false)
        return {
          success: await success,
          message: await message
        }
      } catch (error) {
        console.log(error)
        commit('NOTIFY', error, { root: true })
        return {
          success: false,
          message: error
        }
      }
    },
    async createOrder ({ state, getters }, data) {
      const fechaPago = data.update_time ? dayjs(data.update_time).format('YYYY-MM-DD HH:mm:ss') : ''
      return {
        envio: state.envio,
        paqueteria: state.paqueteria,
        productos: state.productos,
        subtotal: getters.total,
        total: getters.orderTotal,
        forma_pago: state.paymentForm,
        status: getters.orderStatus,
        fecha_pago: fechaPago,
        transaction_id: data.id || ''
      }
    }
  }
}
