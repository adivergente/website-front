import Vue from 'vue'
import Vuex from 'vuex'
import auth from '@/store/auth'
import cart from '@/store/cart'
import products from '@/store/products'
import i18n from '@/plugins/i18n'

import Repository from '@/api/Repository'
const Utils = Repository.get('utils')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    drawer: false,
    cartDrawer: false,
    fixedNav: false,
    notification: '',
    showNotification: false,
    navigationLinks: [
      {
        text: i18n.t('navLinks.home'),
        name: 'Home',
        to: ''
      },
      {
        text: i18n.t('navLinks.products'),
        name: 'Productos',
        to: '/productos'
      },
      {
        text: i18n.t('navLinks.discounts'),
        name: 'Promociones',
        to: '/promociones'
      },
      {
        text: i18n.t('navLinks.quotation'),
        name: 'Cotizacion',
        to: '/cotizacion'
      }
      // {
      //   text: i18n.t('navLinks.news'),
      //   name: 'Noticias',
      //   to: '/noticias'
      // }
    ],
    accountLinks: [
      {
        name: 'Mi Cuenta',
        to: '/mi-cuenta'
      },
      {
        name: 'Cerrar Sesión',
        to: '/logout'
      }
    ],
    estados: [],
    municipios: [],
    localidades: [],
    PAGE_URL: process.env.VUE_APP_SITE_URL
  },
  mutations: {
    SET_DRAWER (state, payload) {
      state.drawer = payload
    },
    SET_CART_DRAWER (state, payload) {
      state.cartDrawer = payload
    },
    SET_FIXED_NAV (state, payload) {
      state.fixedNav = payload
    },
    SHOW_NOTIFICATION (state, payload) {
      state.showNotification = payload
    },
    SET_ESTADOS (state, estados) {
      state.estados = estados
    },
    SET_MUNICIPIOS (state, municipios) {
      state.municipios = municipios
    },
    SET_LOCALIDADES (state, localidades) {
      state.localidades = localidades
    },
    NOTIFY (state, payload) {
      state.notification = payload
      state.showNotification = true
    }
  },
  actions: {
    updateFromLocalStorage ({ commit }) {
      commit('cart/UPDATE_FROM_LOCALSTORAGE')
      commit('auth/UPDATE_FROM_LOCALSTORAGE')
    },
    async getEstados ({ state, commit }) {
      try {
        const { success, data } = await Utils.getEstados(state.auth.token)
        if (await success) {
          const estados = await data.map(obj => obj.nombre)
          commit('SET_ESTADOS', await estados)
          return
        }
        console.log(await data)
      } catch (error) {
        commit('NOTIFY', error)
        console.log(error)
      }
    },
    async getMunicipios ({ state, commit }, estado) {
      if (!estado) return
      console.log('estado', estado)
      try {
        const { success, data } = await Utils.getMunicipios(estado, state.auth.token)
        if (await success) {
          const municipios = await data.map(obj => obj.nombre).sort()
          commit('SET_MUNICIPIOS', await municipios)
          return
        }
        console.log(await data)
      } catch (error) {
        commit('NOTIFY', error)
        console.log(error)
      }
    },
    async getLocalidades ({ state, commit }, { municipio, estado }) {
      console.log('municipio', municipio)
      console.log('estado', estado)
      if (!municipio) return
      try {
        const { success, data } = await Utils.getLocalidades(estado, municipio, state.auth.token)
        if (await success) {
          const localidades = await data.map(obj => obj.nombre).sort()
          commit('SET_LOCALIDADES', await localidades)
          return
        }
        console.log(await data)
      } catch (error) {
        commit('NOTIFY', error)
        console.log(error)
      }
    },
    async solicitarCotizacion ({ commit }, form) {
      try {
        return await Utils.solicitarCotizacion(form)
      } catch (error) {
        commit('NOTIFY', error)
        console.log(error)
      }
    },
    openChat () {
      window.Tawk.maximize()
    }
  },
  modules: {
    auth,
    cart,
    products
  }
})
