export default {
  namespaced: true,
  state: {
    modal: false,
    productInModal: {},
    categories: []
  },
  mutations: {
    SET_CATEGORIES (state, categories) {
      state.categories = categories
    },
    SET_MODAL (state, payload) {
      state.modal = payload
    },
    SET_PRODUCT_IN_MODAL (state, product) {
      state.productInModal = product
    }
  },
  actions: {
    showProductInModal ({ commit }, product) {
      console.log('show product: ', product)
      commit('SET_PRODUCT_IN_MODAL', product)
      commit('SET_MODAL', true)
    }
  }
}
