import router from '@/router'
import Repository from '@/api/Repository'
const User = Repository.get('user')

export default {
  namespaced: true,
  state: {
    token: localStorage.getItem('token') || null,
    user: {}
  },
  getters: {
    isLogged (state) {
      return !!state.token
    },
    hasShippingInfo (state) {
      if (state.user.domicilio) {
        return !!state.user.domicilio.calle &&
          !!state.user.domicilio.num &&
          !!state.user.domicilio.colonia &&
          !!state.user.domicilio.municipio &&
          !!state.user.domicilio.estado &&
          !!state.user.domicilio.codigo_postal
      }
      return false
    }
  },
  mutations: {
    SET_TOKEN (state, token) {
      localStorage.setItem('token', token)
      state.token = token
    },
    SET_USER (state, user) {
      state.user = user
    },
    DESTROY_TOKEN (state) {
      state.token = null
    },
    UPDATE_FROM_LOCALSTORAGE (state) {
      /**
       * Seteamos los valor al nuevo valor en localstorage
       * en caso de no existir(esto pasa al cerrar la sesion), ponemos valor por defecto
       */
      state.token = localStorage.getItem('token') || null
    }
  },
  actions: {
    async login ({ commit, dispatch }, form) {
      try {
        const { success, message, data } = await User.login(form)
        if (!await success) {
          commit('NOTIFY', message, { root: true })
          return {
            success: false,
            message: 'credentials'
          }
        } else {
          commit('SET_TOKEN', await data)
          console.log('data:', await data)
          dispatch('me')
          return {
            success: true,
            message: 'login correcto'
          }
        }
      } catch (error) {
        console.log(error)
        commit('NOTIFY', error, { root: true })
        return {
          success: false,
          message: error
        }
      }
    },
    async register ({ commit, dispatch }, form) {
      try {
        const { success, message, data } = await User.register(form)
        if (!await success) {
          commit('NOTIFY', message, { root: true })
          return {
            success: false,
            message: await message,
            data: await data
          }
        } else {
          commit('SET_TOKEN', await data)
          dispatch('me')
          console.log('data:', await data)
          return {
            success: true,
            message: await message
          }
        }
      } catch (error) {
        console.log(error)
        commit('NOTIFY', error, { root: true })
        return {
          success: false,
          message: error
        }
      }
    },
    async me ({ state, commit, dispatch }) {
      try {
        const res = await User.info(state.token)
        if (await res.success) {
          commit('SET_USER', await res.data)
          return
        }
        dispatch('errorHandler', await res)
      } catch (error) {
        commit('NOTIFY', error, { root: true })
        console.log(error)
      }
    },
    async updateUser ({ state, commit }, form) {
      try {
        const { success, message, data } = await User.updateUser(form, state.token)
        if (!await success) {
          commit('NOTIFY', message, { root: true })
          return {
            success: false,
            message: await message,
            data: await data
          }
        } else {
          commit('SET_USER', await data)
          console.log('data:', await data)
          return {
            success: true,
            message: await message
          }
        }
      } catch (error) {
        console.log(error)
        commit('NOTIFY', error, { root: true })
        return {
          success: false,
          message: error
        }
      }
    },
    async changePassword ({ state, commit }, form) {
      try {
        const { success, message } = await User.changePassword(form, state.token)
        console.log('message:', await message)
        if (await success) {
          commit('NOTIFY', await message, { root: true })
        }
        return {
          success: await success,
          message: await message
        }
      } catch (error) {
        console.log(error)
        commit('NOTIFY', error, { root: true })
        return {
          success: false,
          message: error
        }
      }
    },
    /**
     * Recibe context como parametro, el cual se puede destructurar en:
     * @param {state, commit,dispatch}
     */
    logOut ({ dispatch }) {
      // if (redirectToLogin) {
      //   dispatch('redirectToLogin')
      //   return
      // }
      router.push({
        name: 'Home'
      })
      dispatch('unsetData')
    },
    redirectToLogin ({ dispatch }) {
      dispatch('unsetData')
      router.replace({
        name: 'Login'
      })
    },
    unsetData ({ commit }) {
      localStorage.removeItem('token')
      // localStorage.removeItem('cart')
      commit('SET_USER', {})
      /** Quitar info de cart/envio en caso de haber ingresado al checkout el cual setea en automatico */
      commit('cart/SET_SHIPPING_INFO', {}, { root: true })
      commit('DESTROY_TOKEN')
    },
    errorHandler ({ commit, dispatch }, res) {
      if (res.data.errorLevel === 'token') {
        dispatch('unsetData')
      }
      commit('NOTIFY', res.message, { root: true })
      if (res.data.errorType === 'TokenExpiredError') {
        router.replace({
          name: 'Login'
        })
      }
    }
  }
}
