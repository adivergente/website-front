export default function () {
  return {
    codigo: '',
    clave_interna: '',
    nombre: '',
    equivalencias: '',
    categoria: '',
    subcategoria: '',
    proveedor: '',
    precio: 0,
    autos: [],
    stock: 0,
    servicio_pesado: '',
    popoye: '',
    imagen: [],
    status: '',
    id_promo: ''
  }
}
