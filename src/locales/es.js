const header = {
  logIn: 'Ingresar',
  register: 'Registro',
  hello: 'Hola',
  profile: 'Mi cuenta',
  logOut: 'Salir',
  logOutSession: 'CERRAR SESIÓN',
  phone: 'Teléfono'
}

const specificSearch = {
  btnText: 'Búsqueda específica',
  brand: 'Marca',
  model: 'Modelo',
  year: 'Año',
  motors: 'Motores',
  category: 'Categoría',
  repair: 'Refacción',
  cta: 'Buscar',
  errorMsg1: 'Su búsqueda es similar a la actual'
}

const cartDrawer = {
  headerText: 'Carrito',
  products: 'Tus artículos',
  emptyCart: 'Aún no haz agregado productos.',
  continueShopping: 'Seguir comprando',
  procedCheckout: 'Realizar pedido',
  returnToShop: 'Ir a la tienda'
}

const home = {
  products_1: 'NUESTROS',
  products_2: 'PRODUCTOS',
  productTabs: [
    'Nuevos Productos',
    'Más Comprados'
    // 'En Oferta'
  ],
  news: 'Noticias',
  blog: 'Blog',
  policyIcons: {
    shipping: {
      title: 'Envíos',
      subtitle: 'A todo el país'
    },
    payments: {
      title: 'Pago en línea',
      subtitle: 'Fácil y rápido'
    },
    refunds: {
      title: 'Devoluciones',
      subtitle: '365 días'
    },
    discounts: {
      title: 'Promociones',
      subtitle: 'Descuentos y precios especiales'
    }
  }
}

const footer = {
  links: 'Enlaces importantes',
  privacy: 'Aviso de privacidad',
  cookies: 'Uso de cookies',
  guaranty: 'Garantías y devoluciones',
  sections: 'Apartados',
  discounts: 'Promociones',
  products: 'Productos',
  openingHrs: 'Horario de atención',
  oh1: 'Lun. a Vie.',
  oh2: 'Sab.',
  paymentMethods: 'Formas de pago',
  joinUs: 'Síguenos en'
}

const products = {
  filter: 'Filtrar',
  filters: {
    category: 'Categoría',
    subCategory: 'Subcategoría',
    brand: 'Marca'
  },
  filterText: 'Filtrar resultados por',
  notFound: 'Al parecer no encontramos resultados para tu busqueda.',
  instructions: {
    _1: 'Revisar la ortografía de tu busqueda.',
    _2: 'Utilizar menos palabras o palabras que sean genéricas.',
    _3: 'Utiliza palabras en singular.',
    _4: 'Ingresar solo palabras clave, ejm',
    _5: 'Filtro de aceite para chevy',
    _6: 'Filtro'
  },
  seeProducts: 'Ver todos los productos',
  sort: {
    _1: 'Nombre: A - Z',
    _2: 'Nombre: Z - A',
    _3: 'Menor precio',
    _4: 'Mayor precio',
    _5: 'En liquidación',
    _6: 'En descuento'
  },
  actions: {
    add: 'Agregar',
    buy: 'Comprar ahora',
    request: 'Solicitar producto'
  },
  compatibility: 'Esta pieza es compatible con',
  text_1: 'Envios a <br>todo el país',
  text_2: 'Tarjetas <br>Débito/ Crédito',
  compatibilityTab: 'Compatibilidad',
  details: 'Detalles',
  related: 'Recomendados',
  showingResults: 'Mostrando resultados para',
  seeResults: 'ver resultados',
  goShop: 'Ver productos'
}

const prodDesc = {
  details: 'ver más detalles',
  inStock: 'En stock'
}

const tHeaders = {
  assembler: 'ARMADORA',
  model: 'MODELO',
  years: 'AÑOS',
  motor: 'MOTOR'
}

const orders = {
  search: 'Buscar',
  voucherSent: 'Comprobante enviado',
  seeDetails: 'ver detalle',
  needHelp: 'Necesito ayuda',
  orderDetails: 'Detalles de compra',
  id: 'Folio',
  tHeader: {
    date: 'Fecha',
    id: 'Folio',
    total: 'Total',
    payMethod: 'Forma de pago',
    status: 'Estatus'
  },
  statusText: {
    pagado: 'pagado',
    pendiente: 'pendiente',
    procesando: 'procesando',
    enviado: 'enviado',
    entregado: 'entregado'
  }
}

const resume = {
  paymentError: 'Hubo un problema con tu orden',
  orderResume: 'Resumen de orden',
  orderResumeNote: '*Revisa que todos los datos estén correctos'
}

const oResume = {
  customer: 'Cliente',
  shipping: 'Enviar a',
  payMethod: 'Pago vía',
  status: 'Estatus',
  date: 'Fecha',
  carrier: 'Paquetería',
  tracking: 'No. Guía',
  carrierService: 'Nombre',
  payVoucher: 'Comprobante de pago',
  uploaded: 'Recibido el',
  pending: 'Pendiente de enviar',
  products: 'Productos',
  shippingTax: 'Envío'
}

const cResume = {
  title: 'TU ORDEN',
  products: 'Productos',
  shipping: 'Envío',
  goToResume: 'Finalizar compra',
  pay: 'Realizar compra',
  note: 'Al generar tu orden te indicaremos las instrucciones para completar tu pedido.'
}

const checkout = {
  contact: 'Contacto',
  shipping: 'Envío',
  changeShipping: 'Utilizar otra dirección',
  noProducts: 'Aun no haz agregado productos',
  emptyCart: 'Al parecer tu carrito esta vacio.',
  flushCart: 'vaciar carrito',
  goShop: 'ir a la tienda',
  headerMessage: 'Para completar tu orden es necesario que inicies sesión en tu cuenta',
  loginFormBottomMessage: 'si aún no tienes cuenta con nosotros puedes',
  loginFormBottomMessageLink: 'crear una',
  payMethod: 'Forma de pago',
  cart: 'Tu pedido',
  change: 'modificar',
  details: 'Detalles'
}

const confirmation = {
  successMessage: 'Gracias por tu compra',
  paymentInstructions: 'Para completar tu pedido debes realizar los siguientes pasos',
  paymentInstructions_2: 'Podras dar seguimiento al status de tu orden mediante los detalles de la misma. Si tienes alguna duda recuerda que puedes',
  paymentInstructionsLink: 'contactarnos mediante el chat del sitio',
  successText: 'En breve estarás recibiendo un correo con la información de tu compra. <br>   También puedes acceder a',
  successTextLink: 'tu cuenta',
  successText_2: 'y revisar los detalles.',
  seeProducts: 'ver mas productos'
}

const payments = {
  button: 'Reportar pago',
  select: 'Selecciona tu orden',
  selectPic: 'Seleccionar imagen',
  uploadNote: 'Se muestran solo ordenes que no tienen registrado comprobante de pago.',
  cancel: 'Cancelar',
  send: 'Enviar',
  instructions: {
    _1: 'En el caso de haber seleccionado un medio de pago alternativo a Paypal para tu compra (el cual se acredita al instante), es necesario que sigas los siguientes pasos según sea el caso.',
    _2: 'Si tienes alguna duda recuerda que puedes',
    _2_1: 'contactarnos mediante el chat del sitio'
  },
  // 'Pago en OXXO': 'Pago en OXXO',
  oxxo: {
    // title: 'Pago en OXXO',
    _1: 'Realiza el pago correspondiente en cualquier tienda OXXO al número de cuenta',
    _2: 'Toma una fotografía de tu comprobante o escanealo para enviarlo.',
    _3: 'Ingresa a',
    _3_1: 'tu cuenta',
    _3_2: 'y en las opciones de tu orden selecciona la opción',
    _3_3: 'reportar pago',
    _3_4: 'y agrega tu imagen.',
    _4: 'Una vez tu pago haya sido validado tu compra estará lista.'
  },
  // 'Depósito en banco': 'Depósito en banco',
  deposito: {
    // title: 'Depósito en banco',
    _1: 'Realiza tu pago directo en cualquier sucursal {bank} al número de cuenta {ref}',
    _2: 'Toma una fotografía o captura de pantalla para enviarlo.',
    _3: 'Ingresa a',
    _3_1: 'tu cuenta',
    _3_2: 'y en las opciones de tu orden selecciona la opción',
    _3_3: 'reportar pago',
    _3_4: 'y agrega tu imagen.',
    _4: 'Una vez tu pago haya sido validado tu compra estará lista.'
  },
  // 'Transferencia Interbancaria': 'Transferencia Interbancaria',
  transferencia: {
    // title: 'Transferencia interbancaria',
    _1: 'Realiza tu transferencia a nuestra cuenta',
    _1_1: 'utilizando la CLABE',
    _2: 'Toma una fotografía o captura de pantalla para enviarlo.',
    _3: 'Ingresa a',
    _3_1: 'tu cuenta',
    _3_2: 'y en las opciones de tu orden selecciona la opción',
    _3_3: 'reportar pago',
    _3_4: 'y agrega tu imagen.',
    _4: 'Una vez tu pago haya sido validado tu compra estará lista.'
  }
}

const profile = {
  contact: 'Contacto',
  name: 'Nombre',
  email: 'Email',
  phone: 'Telefono',
  shipping: 'Datos de envío',
  address: 'Dirección',
  street: 'Calle',
  number: 'Número',
  suburb: 'Colonia',
  state: 'Estado',
  city: 'Municipio',
  town: 'Localidad',
  cp: 'Código postal',
  details: 'Referencias'
}

const forms = {
  changePass: 'Cambiar contraseña',
  curPass: 'Contraseña actual',
  newPass: 'Nueva contraseña.',
  nPassHint: 'Longitud mínima {len} caracteres.',
  confNewPass: 'Confirme nueva contraseña',
  update: 'Actualizar',
  quotationDetails: 'Para la cotización',
  quotationImageDetails: 'Puedes agregar una imagen de la pieza a solicitar',
  productReq: 'Pieza solicitada',
  brand: 'Marca',
  model: 'Modelo',
  motor: 'Motor',
  year: 'Año',
  piece: 'Número de parte',
  customerDetails: 'Datos del cliente',
  name: 'Nombre',
  email: 'Email',
  phone: 'Teléfono',
  phoneHint: 'Número a 10 dígitos sin espacios',
  addInfo: 'Información adicional',
  send: 'Solicitar cotización',
  forgotPassForm: {
    done: 'Listo',
    doneText: 'Te hemos enviado un email para que puedas generar una nueva contraseña. Revisa tu bandeja de entrada y sigue las instrucciones para completar el proceso.',
    doneText_1: 'Revisa también la bandeja de spam en caso de no encontrar el email.',
    title: 'Ingresa el email con que inicias sesión.',
    send: 'Enviar email'
  },
  typeYourSearch: 'Ingresa tu busqueda',
  password: 'Contraseña',
  passwordConfirm: 'Confirme su contraseña',
  login: 'Ingresar',
  lastName: 'Apellidos',
  optional: 'opcional',
  signIn: 'Crear cuenta',
  addTown: 'Agregar localidad',
  detailsPlaceholder: 'ej: Calle y media de av. principal, portón cafe...',
  detailsHint: '*Datos que ayuden a ubicar con mayor facilidad el domicilio',
  saveChanges: 'Guardar los cambios',
  requiredHintBottom: '*Todos los campos son requeridos',
  updateShippingForm: 'Actualizar mi información de envío'
}

const validations = {
  req: 'Campo requerido.',
  newPassMinLen: 'Longitud es menor a la requerida',
  newPassDif: 'Contraseña debe ser diferente a la actual',
  passConfirmNotMatch: 'Las contraseñas no coinciden',
  validEmail: 'Ingrese email válido',
  noValidEmail: 'Formato de email no válido',
  typeOnlyLetters: 'Ingrese solo letras',
  onlyNumbers: 'Solo números sin espacios',
  tenDigitsFormat: 'Formato a 10 dígitos',
  minLength: 'Longitud es menor a la requerida',
  cpFormat: 'El código postal es incorrecto'
}

const general = {
  search: 'Buscar',
  seeDiscounts: 'VER PROMOCIONES',
  newsSeeMore: 'Leer más',
  discountsSeeMore: 'Ver productos',
  notFound: {
    text: 'Al parecer hubo un problema con tu busqueda, mientras comprobamos que ha pasado puedes ¡visitar nuestra tienda! ó contactarnos directamente a través del chat.',
    goShop: 'ir a la tienda',
    openChat: 'Abri el chat'
  },
  discounts: 'PROMOCIONES',
  outlet: 'En liquidación',
  forgotPassword: 'Recuperar contraseña',
  needAccount: '¿Necesita una cuenta?',
  createAccount: 'Crear cuenta',
  forgotPasswordQ: '¿Olvidaste tu contraseña?',
  register: 'Registrarse',
  haveAccount: '¿Ya tienes cuenta?',
  resume: 'Resumen',
  welcome: 'Bienvenido',
  account: 'Mis datos',
  paymentInstructions: 'Pagos directos',
  orders: 'Compras',
  prev: 'Anterior',
  next: 'Siguiente',
  promocionesBanner: `<h2 class="header_promociones__title mb-1">
                        POPOYE
                      </h2 >
                      <h1 class="header_promociones__subtitle">
                EL CARRITO DEL AHORRO
                      </h1>
                      <h3 class="header_promociones__subtitle mb-3">
                TRAE PARA TI LOS PRECIOS
                      </h3>
                      <h2 class="header_promociones__title mb-12">
                MÁS BAJOS
                      </h2>`
}

const quotation = {
  info: 'Complete la siguiente información y con gusto le enviaremos la información correspondiente.',
  successMessage: 'Hemos recibido los datos para tu cotización y en breve nos pondremos en contacto contigo para proporcionarte la información que solicitaste.',
  btn: 'Listo',
  sideBarTitle: 'Contacto',
  sideBarSubtitle: 'Estamos a sus ordenes en',
  noProductsFound: 'No se encontrarón productos para esta promoción',
  noActiveQuotations: 'Por el momento no contamos con promociones disponibles'
}

const navLinks = {
  home: 'Inicio',
  products: 'Productos',
  discounts: 'Promociones',
  quotation: 'Cotización',
  news: 'Noticias',
  customers: 'Clientes'
}

export {
  header,
  specificSearch,
  cartDrawer,
  home,
  footer,
  products,
  general,
  tHeaders,
  prodDesc,
  orders,
  resume,
  oResume,
  cResume,
  payments,
  profile,
  validations,
  checkout,
  forms,
  navLinks,
  confirmation,
  quotation
}
