const es = require('./es')
const en = require('./en')
const langs = ['es', 'en']

export {
  langs,
  en,
  es
}
