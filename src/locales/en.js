const header = {
  logIn: 'Login',
  register: 'Register',
  hello: 'Hi',
  profile: 'Profile',
  logOut: 'Logout',
  logOutSession: 'LOGOUT',
  phone: 'Phone'
}

const specificSearch = {
  btnText: 'Specific Search',
  brand: 'Brand',
  model: 'Model',
  year: 'Year',
  motors: 'Engine',
  category: 'Category',
  repair: 'Repair',
  cta: 'Search',
  errorMsg1: 'Similar current search'
}

const cartDrawer = {
  headerText: 'Cart',
  products: 'Products',
  emptyCart: 'Your cart is currently empty.',
  continueShopping: 'Continue shopping',
  procedCheckout: 'Proceed to checkout',
  returnToShop: 'Return to shop'
}

const home = {
  products_1: 'OUR',
  products_2: 'PRODUCTS',
  productTabs: [
    'New',
    'Popular'
    // 'Offers'
  ],
  news: 'News',
  blog: 'Blog',
  policyIcons: {
    shipping: {
      title: 'Shipping',
      subtitle: 'Everywhere'
    },
    payments: {
      title: 'Payments',
      subtitle: 'Fast and easy'
    },
    refunds: {
      title: 'Refunds',
      subtitle: 'Guaranteed purchase'
    },
    discounts: {
      title: 'Discounts',
      subtitle: 'Offers and special prices'
    }
  }
}

const footer = {
  links: 'Links',
  privacy: 'Privacy',
  cookies: 'Cookies',
  guaranty: 'Garanty',
  sections: 'Sections',
  discounts: 'Discounts',
  products: 'Products',
  openingHrs: 'Opening Hours',
  oh1: 'Mon. a Fri.',
  oh2: 'Sat.',
  paymentMethods: 'Payment methods',
  joinUs: 'Join us'
}

const products = {
  filter: 'Filter',
  filters: {
    category: 'Category',
    subCategory: 'Subcategory',
    brand: 'Brand'
  },
  filterText: 'Filter by',
  notFound: 'No products were found matching your selection.',
  instructions: {
    _1: 'Check the spelling of your search.',
    _2: 'Try using synonyms.',
    _3: 'Try singular words.',
    _4: 'Try with keywords or codes like:',
    _5: 'Filtro de aceite para chevy',
    _6: 'Filtro'
  },
  seeProducts: 'See products',
  sort: {
    _1: 'Name: A - Z',
    _2: 'Name: Z - A',
    _3: 'Price: Low to high',
    _4: 'Price: High to low',
    _5: 'Outlet',
    _6: 'Discount'
  },
  actions: {
    add: 'Add to cart',
    buy: 'Buy now',
    request: 'Request product'
  },
  compatibility: 'This part is compatible with',
  text_1: 'Shipping <br>everywhere',
  text_2: 'Debit / Credit <br>Cards',
  compatibilityTab: 'Compatibility',
  details: 'Details',
  related: 'Related',
  showingResults: 'Showing results to',
  seeResults: 'see results',
  goShop: 'Go shop'
}

const prodDesc = {
  details: 'see details',
  inStock: 'In stock'
}

const tHeaders = {
  assembler: 'ASSEMBLER',
  model: 'MODEL',
  years: 'YEARS',
  motor: 'ENGINE'
}

const orders = {
  search: 'Search',
  voucherSent: 'Voucher sent',
  seeDetails: 'see details',
  needHelp: 'Need help',
  orderDetails: 'Order details',
  id: 'ID',
  tHeader: {
    date: 'Date',
    id: 'ID',
    total: 'Total',
    payMethod: 'Payment Method',
    status: 'Status'
  },
  statusText: {
    pagado: 'paid',
    pendiente: 'pending',
    procesando: 'proccesing',
    enviado: 'sent',
    entregado: 'delivered'
  }
}

const resume = {
  paymentError: 'Something goes wrong',
  orderResume: 'Order resume',
  orderResumeNote: '*Check the information before proced'
}

const oResume = {
  customer: 'Customer',
  shipping: 'Shipping',
  payMethod: 'Payment Method',
  status: 'Status',
  date: 'Date',
  carrier: 'Carrier',
  tracking: 'Tracking',
  carrierService: 'Service name',
  payVoucher: 'Payment Voucher',
  uploaded: 'Uploades',
  pending: 'Pending',
  products: 'Products',
  shippingTax: 'Shipping Tax'
}

const cResume = {
  title: 'ORDER DETAILS',
  products: 'Products',
  shipping: 'Shipping',
  goToResume: 'Go to resume',
  pay: 'Pay',
  note: 'Once your order is created we will indicate the following instructions to complete your order.'
}

const checkout = {
  contact: 'Contact',
  shipping: 'Shipping',
  changeShipping: 'Change shipping',
  noProducts: 'Your cart is empty',
  emptyCart: '',
  flushCart: 'empty cart',
  goShop: 'go shop',
  headerMessage: 'Please login to complete your order',
  loginFormBottomMessage: 'if you don\'t have an account you can',
  loginFormBottomMessageLink: 'create one',
  payMethod: 'Payment method',
  cart: 'Cart',
  change: 'change',
  details: 'Details'
}

const confirmation = {
  successMessage: 'Done',
  paymentInstructions: 'To complete your order please follow next steps',
  paymentInstructions_2: 'You can track your order status in order details in your account zone. If you need help you can',
  paymentInstructionsLink: 'contact us by chat',
  successText: 'In a few moments you will received an email with your buy details. <br> Also you can login to',
  successTextLink: 'your account',
  successText_2: 'and check your order details.',
  seeProducts: 'see more products'
}

const payments = {
  button: 'Send voucher',
  select: 'Select order',
  selectPic: 'Select image',
  uploadNote: 'Showing orders with no voucher sent.',
  cancel: 'Cancel',
  send: 'Send',
  instructions: {
    _1: 'If you selected an alternative payment method (not Paypal), it is necessary you follow these simple steps.',
    _2: 'If you have any doubt or need help you can',
    _2_1: 'contact us by chat'
  },
  // 'Pago en OXXO': 'OXXO',
  oxxo: {
    // title: 'OXXO',
    _1: 'Make direct pay at any OXXO store to the account number',
    _2: 'Take a picture of your voucher or scan it to send it.',
    _3: 'Login to',
    _3_1: 'your account',
    _3_2: 'and in order options choose',
    _3_3: 'send voucher',
    _3_4: 'and upload your picture',
    _4: 'Once your payment is confirmed your buy will be completed.'
  },
  // 'Depósito en banco': 'Bank',
  deposito: {
    // title: 'Bank',
    _1: 'Make direct payment in bank <b>{bank}</b> at account number {ref}',
    _2: 'Take a picture of your voucher or scan it to send it.',
    _3: 'Login to',
    _3_1: 'your account',
    _3_2: 'and in order options choose',
    _3_3: 'send voucher',
    _3_4: 'and upload your picture',
    _4: 'Once your payment is confirmed your buy will be completed.'
  },
  // 'Transferencia Interbancaria': 'SPEI',
  transferencia: {
    // title: 'SPEI',
    _1: 'Make transfer to our account',
    _1_1: 'using CLABE(bank transfer id)',
    _2: 'Take a picture of your voucher or scan it to send it.',
    _3: 'Login to',
    _3_1: 'your account',
    _3_2: 'and in order options choose',
    _3_3: 'send voucher',
    _3_4: 'and upload your picture',
    _4: 'Once your payment is confirmed your buy will be completed.'
  }
}

const profile = {
  contact: 'Contact',
  name: 'Name',
  email: 'Email',
  phone: 'Phone',
  shipping: 'Shipping details',
  address: 'Address',
  street: 'Street',
  number: 'No.',
  suburb: 'Suburb',
  state: 'State',
  city: 'City',
  town: 'Town',
  cp: 'Postal code',
  details: 'Details'
}

const forms = {
  changePass: 'Change password',
  curPass: 'Current password',
  newPass: 'New password',
  nPassHint: 'Min length {len} characters.',
  confNewPass: 'Password confirm',
  update: 'Update',
  quotationDetails: 'Quotation details',
  quotationImageDetails: 'Upload an image(optional)',
  productReq: 'Product',
  brand: 'Brand',
  model: 'Model',
  motor: 'Engine',
  year: 'Year',
  piece: 'Piece',
  customerDetails: 'Customer details',
  name: 'Name',
  email: 'Email',
  phone: 'Phone',
  phoneHint: '10 digits number with no spaces between',
  addInfo: 'Additional details',
  send: 'Send quotation',
  forgotPassForm: {
    done: 'Done',
    doneText: 'We sent you an email, now you can generate a new password. Check your inbox and follow the instructions to complete the process.',
    doneText_1: 'Don\'t forget to check spam if email is not in your current inbox.',
    title: 'Type your email.',
    send: 'Send email'
  },
  typeYourSearch: 'Type your search',
  password: 'Password',
  passwordConfirm: 'Password Confirm',
  login: 'Login',
  lastName: 'Last Name',
  optional: 'optional',
  signIn: 'Sign In',
  addTown: 'Add Town',
  detailsPlaceholder: 'Ex: First home at main street, green door... ',
  detailsHint: '*Additional info that helps us to find your place easily',
  saveChanges: 'Save changes',
  requiredHintBottom: '*All inputs are required',
  updateShippingForm: 'Update shipping info'
}

const validations = {
  req: 'Required.',
  newPassMinLen: 'Required min length not match',
  newPassDif: 'Password has to be different than current',
  passConfirmNotMatch: 'Password and password confirm not match',
  validEmail: 'Type valid email',
  noValidEmail: 'Not valid email format',
  typeOnlyLetters: 'Type Only letters',
  onlyNumbers: 'Only numbers with no spaces between',
  tenDigitsFormat: '10 digits format',
  minLength: 'Length is shorter than required',
  cpFormat: 'Format no valid'
}

const general = {
  search: 'Search',
  seeDiscounts: 'SEE DISCOUNTS',
  newsSeeMore: 'See more',
  discountsSeeMore: 'See more',
  notFound: {
    text: 'Apparently there was no result for your search, don\'t worry meanwhile you can visit our store! or contact us directly through the chat.',
    goShop: 'go shop',
    openChat: 'Open chat'
  },
  discounts: 'DISCOUNTS',
  outlet: 'Outlet',
  forgotPassword: 'Forgot password',
  needAccount: 'Need an acoount?',
  createAccount: 'Create an acoount',
  forgotPasswordQ: 'Forgot password?',
  register: 'Register',
  haveAccount: 'Do you have an account?',
  welcome: 'Welcome',
  account: 'Profile',
  orders: 'Orders',
  paymentInstructions: 'Direct payments',
  prev: 'Previous',
  next: 'Next',
  promocionesBanner: `<h2 class="header_promociones__title mb-1">
                        POPOYE
                      </h2 >
                      <h1 class="header_promociones__subtitle">
                THE SAVINGS CART
                      </h1>
                      <h3 class="header_promociones__subtitle mb-3">
                BRINGS TO YOU
                      </h3>
                      <h2 class="header_promociones__title mb-12">
                BEST PRICES
                      </h2>`
}

const quotation = {
  info: 'Please complete next form and we will contact you to provide the requested information.',
  successMessage: 'Your request is sent. We will contact you to provide the information.',
  btn: 'Go home',
  sideBarTitle: 'Contact',
  sideBarSubtitle: '',
  noProductsFound: 'No products found',
  noActiveQuotations: 'No active quotations'
}

const navLinks = {
  home: 'Home',
  products: 'Products',
  discounts: 'Discounts',
  quotation: 'Quotation',
  news: 'News',
  customers: 'Customers',
  resume: 'Resume'
}

export {
  header,
  specificSearch,
  cartDrawer,
  home,
  footer,
  products,
  general,
  tHeaders,
  prodDesc,
  orders,
  resume,
  oResume,
  cResume,
  payments,
  profile,
  validations,
  checkout,
  forms,
  navLinks,
  confirmation,
  quotation
}
