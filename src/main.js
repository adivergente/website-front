import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import VueMeta from 'vue-meta'
import VueNumberInput from '@chenfengyuan/vue-number-input'
import i18n from './plugins/i18n'
import './plugins/pipes'

Vue.config.productionTip = false

Vue.use(VueMeta)
Vue.use(VueNumberInput)
// Vue.use(VuePaginate)

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app')
