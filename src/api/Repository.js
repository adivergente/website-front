import noticias from './Noticias'
import productos from './Productos'
import promociones from './Promociones'
import user from './User'
import utils from './Utils'

const repositories = {
  noticias,
  productos,
  promociones,
  user,
  utils
}
export default {
  get: name => repositories[name]
}
