const BASE_PATH = process.env.VUE_APP_API_URL
export default
{
  all: async () => {
    const res = await fetch(`${BASE_PATH}/noticias/`)
    return await res.json()
  },
  get: async (slug) => {
    const res = await fetch(`${BASE_PATH}/noticias/${slug}`)
    return await res.json()
  }
}
