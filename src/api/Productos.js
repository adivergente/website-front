const BASE_PATH = process.env.VUE_APP_API_URL
// const BASE_PATH = 'http://tiendavirtual.dyndns.org:63'

export default
{
  get: async (endPoint) => {
    const res = await fetch(`${BASE_PATH}/products/${endPoint}`)
    return await res.json()
  },
  getAll: async (page, perPage, sort) => {
    const res = await fetch(`${BASE_PATH}/products?page=${page}&limit=${perPage}&sort=${sort}`)
    return await res.json()
  },
  generalSearch: async (data, page, perPage, sort) => {
    const res = await fetch(`${BASE_PATH}/products/search/d?search=${data}&page=${page}&limit=${perPage}&sort=${sort}`)
    return await res.json()
  },
  categories: async () => {
    const res = await fetch(`${BASE_PATH}/products/categorias`)
    return await res.json()
  },
  getCategoriesForSpecificSearch: async (data) => {
    const res = await fetch(`${BASE_PATH}/products/specific-search/categorias`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
    return await res.json()
  },
  subCategories: async (category) => {
    const res = await fetch(`${BASE_PATH}/products/specific/tipo/${category}`)
    return await res.json()
  },
  brands: async (brand) => {
    const res = await fetch(`${BASE_PATH}/products/specific/marca/${brand}`)
    return await res.json()
  },
  outlet: async () => {
    const res = await fetch(`${BASE_PATH}/products/outlet`)
    return await res.json()
  },
  recommended: async () => {
    const res = await fetch(`${BASE_PATH}/products/recomendados`)
    return await res.json()
  },
  dataForSpecificSearch: async () => {
    const res = await fetch(`${BASE_PATH}/products/primero`)
    return await res.json()
  },
  // TODO: Validar para que se usa esta ruta
  getFilteredResults: async (data) => {
    const res = await fetch(`${BASE_PATH}/products/specific/filtros`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
    return await res.json()
  },
  specificSearch: async (data) => {
    const res = await fetch(`${BASE_PATH}/products/specific`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
    return await res.json()
  },
  search: async (data) => {
    const res = await fetch(`${BASE_PATH}/products/show/p?ref=${data}`)
    return await res.json()
  }
}
