const BASE_PATH = process.env.VUE_APP_API_URL

export default
{
  get: async () => {
    const res = await fetch(`${BASE_PATH}/ad-usuarios/datos2`, {
      method: 'POST',
      headers: {
        // Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ id: 'ernestoascent@gmail.com' })
    })
    return await res.json()
  },
  login: async (data) => {
    const res = await fetch(`${BASE_PATH}/users/login`, {
      method: 'POST',
      headers: {
        // Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    return await res.json()
  },
  register: async (data) => {
    const res = await fetch(`${BASE_PATH}/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    return await res.json()
  },
  updateUser: async (data, token) => {
    const res = await fetch(`${BASE_PATH}/users/update`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(data)
    })
    return await res.json()
  },
  changePassword: async (data, token) => {
    const res = await fetch(`${BASE_PATH}/users/update-password`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(data)
    })
    return await res.json()
  },
  sendResetPassword: async (data) => {
    const res = await fetch(`${BASE_PATH}/users/password/send-reset-token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    return await res.json()
  },
  validatePasswordResetToken: async (token) => {
    const res = await fetch(`${BASE_PATH}/users/password/reset/${token}`)
    return await res.json()
  },
  changePasswordReset: async (data) => {
    const res = await fetch(`${BASE_PATH}/users/password/reset`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    return await res.json()
  },
  info: async (token) => {
    const res = await fetch(`${BASE_PATH}/users/me`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
    return await res.json()
  },
  ordenes: async (token) => {
    const res = await fetch(`${BASE_PATH}/ordenes`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
    return await res.json()
  },
  saveOrder: async (order, token) => {
    const res = await fetch(`${BASE_PATH}/ordenes`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(order)
    })
    return await res.json()
  },
  uploadPaymentVoucher: async (form, token) => {
    return await fetch(`${BASE_PATH}/ordenes/upload-comprobante`, {
      method: 'POST',
      headers: {
        // 'Content-Type': 'multipart/form-data',
        // 'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: form
    }).then(res => res.json())
  },
  emailAvailable: async (email) => {
    const res = await fetch(`${BASE_PATH}/users/email-available`, {
      method: 'POST',
      headers: {
        // Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ email })
    })
    return await res.json()
  }
}
