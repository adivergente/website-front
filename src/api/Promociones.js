const BASE_PATH = process.env.VUE_APP_API_URL

export default
{
  get: async () => {
    const res = await fetch(`${BASE_PATH}/promociones/all`)
    return await res.json()
  },
  getPromo: async (id) => {
    const res = await fetch(`${BASE_PATH}/promociones/search/${id}`)
    return await res.json()
  }
}
