const BASE_PATH = process.env.VUE_APP_API_URL

export default {
  getEstados: async (token) => {
    const res = await fetch(`${BASE_PATH}/utils/estados`, {
      headers: {
        // Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
    return await res.json()
  },
  getMunicipios: async (estado, token) => {
    const res = await fetch(`${BASE_PATH}/utils/municipios/${estado}`, {
      headers: {
        // Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
    return await res.json()
  },
  getLocalidades: async (estado, municipio, token) => {
    const res = await fetch(`${BASE_PATH}/utils/localidades/${municipio}/${estado}`, {
      headers: {
        // Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
    return await res.json()
  },
  solicitarCotizacion: async (form) => {
    return await fetch(`${BASE_PATH}/cotizacion`, {
      method: 'POST',
      // headers: {
      //   'Content-Type': 'application/json'
      // },
      body: form
    }).then(res => res.json())
    // return await res.json()
  },
  getFormasPago: async () => {
    const res = await fetch(`${BASE_PATH}/formas-pago`, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
    return await res.json()
  }
}
