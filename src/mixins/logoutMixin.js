export const logoutMixin = {
  methods: {
    logOut () {
      // Si se esta en ruta protegida redirige a login sino ejecuta comportamiento por defecto
      // const redirectToLogin = this.$route.meta.authenticated || false
      // this.$store.dispatch('auth/logOut', redirectToLogin)
      this.$store.dispatch('auth/logOut')
    }
  }
}
