/**
 * Se incluye en todos los componentes que tengan que hacer uso de dtos de usuario requeridos
 */
export const authMixin = {
  created () {
    if (!this.$store.getters['auth/isLogged']) {
      this.$router.replace({ name: 'Login' })
    }
  }
}
